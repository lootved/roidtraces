package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func main() {
	input := "data/input.trace"
	output := ""
	flag.StringVar(&input, "i", input, "input file to process")
	flag.StringVar(&output, "o", output, "output path")
	flag.Parse()

	if output == "" {
		output = input + ".cln"
	}

	f, err := os.Open(input)
	check(err)
	defer f.Close()
	sc := bufio.NewScanner(f)

	sc.Split(bufio.ScanLines)

	res := []byte{}

	for sc.Scan() {
		bline := sc.Bytes()
		if isEnd(bline) {
			break
		}
		idx, ok := matchesFilter(bline)
		if ok {
			res = append(res, bline[idx:]...)
			res = append(res, '\n')
		}

	}
	os.WriteFile(output, res, 0644)

}

func matchesFilter(bline []byte) (int, bool) {
	size := len(bline)
	idx := 0
	for idx < size && bline[idx] != '\t' {
		idx++
	}

	if idx == size {
		//log.Println("unable to find space")
		return -1, false
	}
	idx++
	// log.Println("val at idx", idx, " is", string(bline[idx]))

	expected := []byte("PKG.ID.TO.TRACE")
	esize := len(expected)
	if size-idx < esize {
		// log.Println("remaining size is too small", size-idx, esize)
		return -1, false
	}
	for i := 0; i < esize; i++ {
		if bline[i+idx] != expected[i] {
			return -1, false
		}
	}

	return idx, true

}

func isEnd(line []byte) bool {
	return line[0] == '*' && line[1] == 'e' && line[2] == 'n' && line[3] == 'd'
}

func check(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
